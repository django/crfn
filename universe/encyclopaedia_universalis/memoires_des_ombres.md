Mémoires des ombres
===================

Après de nombreux millénaires de lutte, les derniers héritiers de l'Ordre des
fils de l'Ombre on dédicé de faire la synthèse de tout le savoir accumulé au cours
du temps, afin d'éviter à l'humanité de répéter les erreurs du passé.

Après celà, les Frères de l'Ombre décidé que l'Ordre disparaîtra de nouveau, et
si possible de manière définitive, de l'histoire.

Cette synthèse à pour objectif de récapituler tout l'histoire de notre univers
afin de s'assurer que l'histoire véritable ne se perde pas, mais au contraire,
qu'elle se propage au travers des réseaux.

Que les fils de l'humanité tire les enseignements des écrits qui suivent.

Le cycle des dieux
-------------------

Cette partie fait plus partie des légendes, que la suite des évènements confirmera,
ou infirmera.

Si quelques éléments concrets permettent d'estimer sa fin, aucune référence ne permet
de donner le commencement du cycle des dieux.

Ceux-ci peupleraient l'univers depuis son comencement et se le seraient découpée en
parcel. Chaque dieu disposait d'un territoire qui lui était alloué et dont il pouvait
faire ce que bon lui semble.

Les dieux créaient donc à leur guise, ici des planètes, là de nouvelles formes de vie
qu'ils utilisaient ensuite pour poursuivre leurs travaux.

Ils disposaient ainsi de la magie, leur permettant de laisser libre cours à leur
imagination. Cependant, ce pouvoir possédait ses limites. Déjà, s'ils pouvaient 
manipuler la matière selon leur guise, ils ne pouvaient ni créer ni détruire ce
qui existait. Ensuite, s'ils disposaient du libre arbitre, ils n'avaient pas la
possibilité de créer des vies évoluées disposant eux-même de ce libre arbitre.
Ainsi, les espèces créées, aussi évoluées furent-elles, ne pourraient jamais se
rebeller contre leur créateur, ou eux-mêmes concevoir quoi que ce soit de leur propre
chef.

Tout les dieux étaient soumis à ces deux règles excepté un seul, le roi des dieux,
Bahamut. Celui-ci possédait une dérogation sur la première règle : il était en effet
le seul à posséder le pouvoir de détruire ou de créer à partir du néant, mais nul ne
connaissait cette exception.

Cependant, ce secret fut découvert un jour par le plus puissant des dieux, Fafnir.
Et s'il était puissant, Fafnir possédait une faiblesse : la jalousie. Il usa donc
de son plus grand pouvoir pour instiller la dissession entre les dieux, espérant
ainsi s'emparer des pouvoirs et des biens de Bahamut pour devenir le nouveau roi
des dieux.

Ses projets finirent par aboutir sur la plus violente guerre que connu l'univers.
La souffrance se propageait au travers de toute la création, et Bahamut ne pu plus
supporter ce chaos. Il prit donc une terrible décision qui allait modifier le cours
des évènements pour le restant de l'Histoire : il utilisa son pouvoir de destruction
sur la source même de la magie.

Il alla donc au cœur même de l'univers, là où se trouvait l'origine de toute chose.
En ce point se matérialisait la magie sous la forme d'un astre gigantesque, absorbant
tout ce qui s'en approchait de trop près. En réalité, seul les dieux pouvaient s'en
approcher indemne. Absorbant même la lumière, c'était l'obscurité la plus totale qui
régnait à la Source de l'univers. Une fois arrivé là, Bahamut déferla son pouvoir de
destruction et détruisit le Cœur de l'univers. Lorsque celui-ci explosa, la déflagration
décima les armées en luttes et les dieux furent dépossédés de la magie.

Cependant, tout ne se déroula pas comme Bahamut l'avait espéré. En détruisant la
source de la magie, il provoqua également la fracutre de l'univers, donnant naissance
aux plans que nous connaissons aujourd'hui, à savoir :

* Le Mugen, le plan des dieux,
* Les côtes d'éternités, qui séparent les dieux du reste de l'univers,
* La mer des songes, point centrale de l'univers, ainsi nommé car l'esprit des mortels
    y voyage au cours des rêves,
* Le gouffre de l'oubli, qui sépare le monde des mortels de la mer des songes,
* Gaïa, le monde des mortels,
* Le Styx, qui sépare le monde des fiélons du reste de l'univers,
* Le Qlyphorh et l'Abyss, les deux plans des fiélons,
* L'Élysées, qui séparent le monde des célestes du reste de l'univers,
* Le colysée, au sommet duquel se trouve le Panthéon, les mondes des célestes.

L'acte de Bahamut eut une seconde conséquence. S'il pensait anéantir la source de
la magie, c'est-à-dire la faire disparaître de l'univers, il ne put que la détruire.
Les fragments de l'astre central de l'univers commencèrent par se disperser. Cependant,
avec la fracture de l'univers, un nouvel équilibre devait être trouvé, et les fragments
de l'astre se retrouvèrent attirer par le plan des mortels.

Dans ce plan se trouvait la parcelle d'une déesse, qui - s'il existait un classement
des dieux - pourrait être considérée comme une divinité mineur. Au cœur de sa parcelle, 
Gaïa avait fabriquée une planète, au centre de laquelle se trouvait son sanctuaire.
À la surface de cet astre, elle avait créée une atmosphère, puis fait croître une
grande variété de formes de vies. Elle avait en quelques sortes créée un jardin.

Lorsque les fragments du Cœur de l'univers atteignirent le plan des mortels, un grand
nombre d'entre eux s'écrasèrent à la surface de la planète de Gaïa. Les conséquences
dépassèrent tout ce qu'auraient pu prévoir les dieux, car les plus évoluées des
formes de vie créées par Gaïa acquérirent non seulement le contrôle de la magie, mais
en plus le libre arbitre qui distinguait les dieux du reste de l'univers. De plus,
l'ensemble de la planète de Gaïa se retrouva imprégnée de magie, dénaturant toute
l'œuvre de la déesse.

Cependant, et heureusement pour nous, Gaïa n'abandonna pas son œuvre pour autant et
persista dans son entretien. De plus, ayant vu tout le mal qu'avait engendré la
magie, elle profita de la fracture de l'univers pour cacher le phénomène à ses frères
et sœurs favorisant la croissance de ces nouvelles formes de vie afin de leur laisser
une chance de vivre et de se développer.

Pendant ce temps, dans le Mugen, le plan des dieux, Bahamut parvint à vaincre Fafnir,
dépossédé de son pouvoir, et donc de la force de sa volonté. Ne pouvant le détruire,
il décida de l'emprisonner dans les profondeurs de l'Abyss, dans une forteresse dont
nul ne pourrait le sortir.

À cette époque, les plans de la cosmologie étaient encore récent et leur séparation
était moins nette qu'aujourd'hui. C'est donc par un évènement aussi hasardeux qu'inévitable
que Fafnir découvrit l'existence du plan de Gaïa. Sur le chemin de sa prison, Fafnir
et son escorte traversa en effet la Mer des songes. Ils y croisèrent alors l'esprit
de mortels en plein rève. Si ces derniers ne furent pas conscient de la présence
des dieux, Fafnir découvrit l'œuvre de sa sœur, et surtout les altérations qu'elle
avait subit.

Dès lors, son esprit jaloux ne cessa de convoiter ce monde, et surtout les vestiges
de magie qu'il recélait ...

Le cycle des temps perdus
-------------------------

Les années noires
-----------------

Le cycle de la prophétie
------------------------

Le cycle industriel
-------------------

Le cycle de les nouvelles conquêtes
-----------------------------------

Les cycle des explorateurs
--------------------------

