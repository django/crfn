Notes concernant les moyens de propulsion réalistes pour de la SF -> du Space Opera
===================================================================================

Ressources pour les solutions déjà envisagées :
-----------------------------------------------

http://www.savoirs.essonne.fr/dossiers/lunivers/exploration-spatiale/la-propulsion-nucleaire-dans-lespace/

http://www.savoirs.essonne.fr/thematiques/lunivers/exploration-spatiale/conquete-spatiale-les-lanceurs-du-futur/

Combustion chimique de liquide : poussée de 1000 à 10 000 000 N (accéleration
très forte, vitesse faible ; cf. Modes de propulsion actuel). Mobilité limitée.
Faible autonomie, notamment à poussée max (utilisé pour la sortie de
l'atmosphère). Terre / Mars en 6 mois. 

Propulsion nucléo/électrique : Poussée identique, mais autonomie quasi illimitée
(accélération très lente et grande vitesse). Grande mobilité. Grande autonomie.

Propulsion thermo nucléaire : poussée max 3~4 fois plus élevée et autonomie
quasi illimitée (nécessite du carbure d'uranium, soit ~20ans de recherche
supplémentaire par rapport au nucléaire classique ; accélération forte, grande
vitesse). Mobilité faible, mais peut être nettement accrue au prix de recherches
supplémentaires. Grande autonomie. Des recherches supplémentaires pourrons
aboutir à des améliorations sur la poussée max (combustible dont la température
max est plus élevée), la mobilité (assistance informatisée + solutions pour
évacuer la chaleur). Terre / Mars en 8 semaines au début (divisible par 3~4 en
passant de la fission à la fusion nucléaire).

Propulsion thermo nucléaire avec allumage par réaction anti matière : ce procédé
permet de faire disparaître le problème de la lenteur du démarrage d'un
propulseur thermo nucléaire (jusque là, les propulseurs seraient obligé de
décoller avec un propulseur chimiques, également à cause du danger d'un
propulseur atomique en atmosphère). Hormis la poussée initiale, toute les autres
caractéristiques sont les mêmes.

Propulsion à anti matière : (Source : http://www.revelationsweb.com/ovni-fabrications/156-propulsion-anti-gravitationnelle-et-effet-inertique)
Poussée jusqu'à 300 fois supérieure par rapport au propulseur thermo nucléaire à
fission. Terre / Mars en ~1h15. Remarques : certaines théories disent que la
poussée max est proportionnelle à la quantité d'anti matière consumée (quelques
centaines de grammes pour produire l'équivalent d'un propulseur thermo
nucléaire). D'autres part, les moyens disponibles sur Terre (accélérateurs de
particules) ne pourrons certainements pas produire plus de quelques milli
grammes chaque année, ce qui est clairement insuffisant pour parler de
rentabilité. D'autres solutions devront être envisagés.

Propulsion à anti gravité, variante du propulseur à anti matière pour générer un
champ magnétique en rotation suffisante pour permettre au vaisseau de
s'affranchir des effets de gravitation par la production d'antigravité. Ce
procédé pourrait permettre d'atteindre la vitesse de la lumière, et à plus long
terme d'aller un peu plus vite, grâce à la protection par le champ magnétique
(permettrait d'exploiter au max les performances accessibles grâce à l'anti
matière). Cette technologie permettrait de voyager d'un point à un autre d'un
système solaire en quelques minutes maximum. La mobilité dépend de la vitesse
souhaitée. Très grande autonomie. 

Remarque : la quantité d'énergie nécessaire, autant pour la production que pour
l'utilisation, des technologies listées ci dessus augmente de façon
exponentielle, ce qui implique des progrès astronomiques à réaliser entre chaque
étape. Cela peut ralentir la progression d'une solution à une autre.