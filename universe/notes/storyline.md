Grandes lignes de l'histoire du CRFN
====================================

Bien avant le commencement du premier cycle, une race était parvenue à un tel
niveau de maîtrise des technologies et de la magie qu'elle avait commencée à
coloniser plusieurs galaxies. Pour en arriver là, ces êtres avaient réussit à
voyager via une cinquième dimension hors du temps et de l'espace.
Cette cinquième dimension correspond en fait à la mer des songes, l'abysse, le
colisée, etc.

Cette antique civilisation s'enlisa dans une guerre civile qui faillit aboutir
à la destruction de l'univers tout entier.
Ils avaient alors établie leur capitale en mer des songes, la plaçant au
carrefour de tout les mondes de l'univers. En effet, il était possible d'aller
n'importe où dans l'univers depuis cette 4e dimension par un système assimilable
aux portails temporels qui permettent de s'affranchir du temps de trajet.

Remarque : la notion de portail temporel découverte, puis maîtrisés au cours du
6e cycle pourrait être remaniée pour utiliser un passage au travers de cette 5e
dimension. Le principe doit être développée, car l'explosion de l'univers en
plusieurs plans d'existence ne doit pas rendre impossible l'utilisation de ce
mode de déplacement (i.e. étendre la notion de 5e dimension, dont la mer des
songes, le Qlyphoth etc. ne seraient plus qu'une partie).

Tant que la cité était dans un équilibre raisonnable, le fait de l'avoir placée
au cœur de la mer des songes avait relativement peu de conséquence, mais lorsque
la guerre ravagea la cité, ils découvrirent à leurs dépends que c'était une
dimension fragile qui n'était pas faite pour les vivants.
Cette dimension était en fait le territoire des rêves (et cauchemars) en même
temps que le dernier endroit où passaient les âmes des défunts avant de quitter
l'univers.

L'une des origines du conflit, c'est la dualité entre Mana et Anti-Mana. La
première était l'essence même de la civilisation des pères des dieux. La seconde
existait partout où régnait le vide absolu.  Arrivé à ce qui fut alors le
paroxysme de leur puissance, les pères des dieux cherchèrent à contrôler
l'Anti-Mana, ce qui leur était impossible par essence même.  Les recherches
qu'ils firent alors aboutirent à la création d'aberrations et de formes de
(non-)vie d'Anti-Mana qui échappèrent à leur contrôle.  L'instant où l'univers
faillit réellement disparaître, c'est celui où des créations réalisées à partir
d'Anti-Mana furent introduites dans la capitale, et en Mer des Songes.
L'Anti-Mana pur au contact de Mana pur abouti à l'annihilation pure et simple
des deux. Heureusement, seules des quantités infimes d'Anti-Mana furent
introduites en mer des songes, mais la cinquième dimension avait alors commencée
à s'effondrer sur elle-même.

Arrivé au bord de l'extinction, les derniers survivants de cette ancienne
civilisation décidèrent de sceller l'essence de leurs pouvoirs dans un astre,
qui le diffuserait à petite dose.  Ils donnèrent ensuite le contrôle de ce
pouvoir à leurs enfants, et effacèrent toute l'histoire de leur civilisation
pour que l'univers exsangue puisse se reconstruire. Ils en sauvegardèrent quand
même le savoir en un de leurs fils avec pour responsabilité la gestion des
autres enfants.  C'est ainsi que débutèrent Bahamut, Gaïa, Fafnir et les autres
dieux.  Ainsi, seul Bahamut connaissait leur origine, et seulement de façon
inconsciente.  De la même façon, l'astre laissé en héritage par la civilisation
disparue devint le cœur de la nouvelle galaxie, et le Cœur de Mana.  Enfin, la
cinquième dimension put retourner à son état originel, et les dieux évitaient
d'utiliser la mer des songes pour voyager, la puissance dont ils avaient hérité
étant suffisante pour leurs besoins en déplacement. (À developper: seule l'une
d'entre eux garda la capacité de voyager via la mer des songes).  Remarque: ils
ne pouvaient y accéder directement, car la 4e dimension se trouvait au cœur du
Cœur de Mana. (Notion à développer)

Les dieux devaient également disposer de peu d'humanité, afin de leur éviter
toute dérives. Mais c'est peut-être parce qu'ils avaient hérité des gènes d'une
race douée d'humanité qu'ils finirent par en développer une, au cours des
millénaires.  Le fait qu'ils étaient chacun rattaché à l'un des 5 éléments à
sans doute également joué, influant sur leur conception de l'univers.  C'est
ainsi que Fafnir en arriva à chercher la puissance, puis le pouvoir, et Gaïa de
développer sa créativité.  Seul Bahamut a toujours eu une forme embryonnaire
d'humanité à cause de ses responsabilités, mais il a beaucoup moins évolué que
les autres, sur ce point (Cette faible évolution est peut-être également du au
fait qu'il est lié aux deux éléments paritucliers que sont l'Ordre et le Chaos).

Ainsi, chaque dieu commença à travailler sur leur tâche favorite, à savoir:
créer des mondes. Pour cela, ils s'éloignèrent assez du Cœur de Mana et les uns
des autres pour ne pas s'influencer les uns les autres, puis ils créèrent
étoiles et systèmes stellaires à partir des amas de matière primordiale qu'ils
trouvèrent sur place.  C'est ainsi que furent créé les amas stellaires.

L'étape suivante du récit correspond au 1er cycle.

Lorsque Bahamut, cherchant à endiguer les ambitions de Fafnir, détruisit le Cœur
de Mana, il provoqua une déchirure de l'univers, le mettant de nouveau en péril.
Bien qu'il ne le sut, il avait ainsi créé trois plans: un où se trouvaient les
dieux, un où Gaïa et son monde continuaient leur chemin, et un avec la dimension
de la mer des songes, alors libérée de son isolement (depuis sont enfermement
dans le Cœur de Mana, il n'était accessible qu'une fois mort).  Bahamut prit
alors conscience de son erreur, réveillant en lui d'antiques souvenirs, et tout
ce à quoi il visa par la suite fut de rétablir l'équilibre de l'univers.
Cependant, le hasard fit que les fragments du Cœur de Mana se retrouvèrent dans
l'autre plan, celui où Gaïa se trouvait. Sa seule solution fut alors de laisser
les trois plans retrouver leur équilibre, mais les ambitions de Fafnir n'avaient
pas cessées de menacer l'univers malgré son emprisonnement dans les profondeurs
de l'Abyss. C'est que dans son aveuglement, Fafnir pensait pouvoir acquérir
suffisamment de puissance pour survivre au déchirement de l'univers, et pouvoir
ainsi recréer son monde sur les ruines de l'ancien.

Il est à noter que le choc Mana / Anti-Mana, qui peut provoquer la destruction
de l'univers s'il a lieu dans la 5e dimension, et la déchirure de l'univers
provoquée par l'explosion du Cœur de Mana n'ont rien de commun. Dans le premier
cas, l'univers cesse d'exister ; dans le second cas, il est déchiré en une
multitude de plans, ce qui n'est pas une fatalité en soi.

La suite correspond aux événements du 3e cycle: Fafnir retrouve la trace du Cœur
de Mana dans le monde de Gaïa, mettant en péril l'équilibre des plans, mais il
sera définitivement défait par un groupe de mortels qui auront acquis la
maîtrise des composantes élémentaires du Mana, et notamment l'Ordre et le Chaos.
Parmis eux se trouvera un mortel (Lucian) qui aura alors atteint un niveau de
quasi immortalité. Lui et son épouse, qui est une immortelle créée par Gaïa,
iront vivre au sommet du Colysée, où ils deviendront des "quasi-dieux". Là, ils
continuerons de croître en puissance pour finalement être acceptés au rang de
dieu.

Après la défaite de Fafnir, les dieux pensaient avoir définitivement la paix
avec les menaces sur l'univers, mais les mortels continuèrent de croître en
puissance, acquérant de nouvelles connaissances et la maîtrise de nouvelles
connaissances. Parmi leurs plus grandes prouesses, on peut notamment compter la
création de quelques races artificielles, qui réussirent à devenir une race à
part entière lorsque les chercheurs testèrent l'utilisation du Cœur de Mana sur
leurs créations. À partir de là, cette nouvelle race commença à se développer de
façon totalement indépendante. On peut également compter les cyborgs, qui sont
des mortels qui ont usé de la technologie pour modifier leur organisme (cf. Nano
technologies). D'autres races feront également leur apparition, par
l'utilisation et le développement de la maîtrise du mana.

Avec ces évolutions, de nombreuses factions vont se former au travers de la
galaxie: une alliance mortels/immortels qui cherchent un nouvel équilibre des
forces; une faction d'immortels qui veulent s'imposer comme leaders des mortels,
une faction de mortels qui veulent s'affranchir des immortels, une de mortels et
cyborgs qui veulent devenir de nouveaux dieux,... Sans compter ceux qui veulent
faire du bénéfice sur la situation, ceux qui n'en ont rien à faire, ceux qui
cherchent le pouvoir pour leur propre compte, etc.

À ce stade du récit, Bahamut cherche avant tout la sauvegarde de l'univers, Gaïa
veut protéger sa création, les Fils de l'Ombre, organisation secrète héritée du
3e cycle, cherche à lutter contre les dérives des mortels, et ils auront leurs
antagonistes avec ceux qui cherchent à combiner les pouvoirs de la magie et de
la technologie pour devenir de nouveaux dieux.

La dernière "prouesse" des races de mortels sera de tenter de faire usage de
l'Anti-Mana pour de nouvelles expériences, mais ils réussiront surtout à
réveiller un mal ancien qui va commencer à consommer la galaxie.

Le dernier acte du 6e cycle consistera en une lutte sur la corde pour empêcher
les aberrations d'Anti-Mana d'atteindre la 5e dimension et ainsi détruire
l'univers...  Une ouverture possible: la sauvegarde se fera par un sacrifice qui
se traduira par le déchirement de l'univers en une multitude de plans. Ce
faisant, la 5e dimension deviendra inaccessible autrement qu'en mourant, et
ainsi, l'Anti-Mana ne sera plus une menace pour l'univers. Les différents plans
pourrons développer leurs propres caractéristiques, mais cela n'est plus du
ressort de notre chronologie.

De l'ensemble de ce récit, nul n'en a conscience dans l'univers. Celui qui en
sait le plus, c'est Bahamut, même s'il n'a pas conscience de tout. D'une façon
très schématique, il a été conçu pour assurer son rôle, et saura toujours qui ou
quoi pourrait menacer l'univers (excepté pour sa déchirure, qu'il voit comme une
perte d'intégrité de l'univers alors que ça ne le détruit pas). Finalement,
aucun des dieux n'aura conscience d'avoir été créé un jour, et leurs souvenirs
remonterons au mieux au moment où ils se sont dispersés pour recréer le galaxie.
En d'autres termes, ils se verront eux-mêmes comme les créateurs de la galaxie
(et non de l'univers, même si Fafnir pourrait finir par s'en convaincre), d'où
le nom qu'ils se sont donné, qui signifie "Bâtisseur de mondes".

