contrôle du mana par l'industrie.
=================================

Sur ce cycle, la magie n'existe pas, et le mana est utilisé comme une source
d'énergie, comme le pétrole,... Seulement, sa puissance est certes
phénoménale, mais surtout incontrôlable.

Les croyances
-------------

Au début du second cycle, Gaïa décida de retirer ses esprits de la surface,
pour laisser toute la place aux nouveaux êtres qui commençaient à se développer
depuis l'arrivée du cœur de Mana.

Cependant, il est resté dans la conscience collective le souvenir de Gaïa,
déesse suprême, et des esprits habitants la nature.

À la fin du cycle, Freya perçut les avancées des mortels dans la maîtrise du
mana, et notamment l'impact sur l'environnement, elle renvoya donc quelques
esprits pour se maintenir au courant de l'activité des mortels. C'est de cette
façon que les ancêtres de Lucian von Ruthven vont avoir l'occasion de croiser
une première fois Freya.

Le mana au second cycle
-----------------------

Le mana est contenu sous la forme d'artéfacts conférent des capacités spéciales
aux objets auxquels ils sont liés, sous forme brute (extraite des artéfacts) ou
sous forme de minerai, d'où il peut également être extrait.

Le problème concerne la façon de stocker le mana brut, que seul le système de
piles de mana parviendra à résoudre (dans une certaine mesure).

Fin du Cycle industriel
-----------------------

### Les réacteurs de mana

Lse réacteurs de mana, mis au point quelques décennies plus tôt, on besoin du
mana extrait des artéfacts et minerais pour fonctionner. Cependant, les
réacteurs en ont besoin en des quantités telles que le stockage d'artéfacts et
de minerai pose problèmes. Malheureusement, le stockage de mana brut pose
problème, et il faut littéralement brûler le minerai directement dans le
réacteur pour pouvoir l'utiliser.

Un étrange phénomène s'observe également autour des réacteurs de mana.
L'orsqu'ils brûlent du minerai ou des artefacts, ils dispèrsent d'infimes
quantités de mana dans l'environnement autour de la centrale. Les quantités de
mana ainsi dispersées sont infimes en comparaison de ce qu'il y aura au 3è
cycle, Cependant, d'étonnantes mutations seront observées sur la faune et la
flore. Du point de vue du 3è cycle, ce sont justes des formes de vie qui
développent des aptitudes magiques, mais pour les contemporains du second
cycle, ces mutations seront perçues comme des anomalies.

Remarque : On peut imaginer que les scientifiques de la fin du second cycles
auront déjà commencé à faire quelques expérimentations dans le plus grand
secret, mais qui n'aboutirait qu'à la création de mutants instables.

Les réacteurs de mana présentent un tel intérêt stratégique, qu'ils font
l'objet d'une surveillance extrême, et tout les phénomènes liés à leur
exploitations seront mal connus.
Leur coût fait également que seuls de puissantes sociétés pourrons les
construire.

### Les piles de mana

La mise au point de piles de mana sera l'aboutissement de décennies de
recherches pour trouver une solution de stockage du mana brut, et sera l'enjeux
de pouvoir des grandes puissances de la fin du second cycle.

Au départ, l'évolution tient essentiellement dans la concentration du mana dans
les minerais (qui sera d'ailleurs le moyen utilisé pour créer des artefacts
artificiellement. À noter qu'en fin de second cycle, les scientifiques ont
encore du mal à contrôler le résultat sur les caractéristiques des artéfacts
produits). La limite de la concentration de mana dans des artéfacts sera vite
atteinte :  ceux-ci deviennent rapidement instables.

La mise au point des piles de mana découlera de la mise au point des champs
magnétiques (par un certain Karl Tesla), qui permettra de repousser le seuil
d'instabilité.

Remarque : les piles de mana sont tellement grosses, qu'il faudra un semi
remorque pour en transporter une seule. L'encombrement découle essentiellement
du niveau de maîtrise des champs magnétiques des débuts. Leur taille sera
progressivement réduite, mais restera quand même relativement massive.

Les piles de mana sont stables mais encore limitées. L'origine du conflit sera
la mise au point de réacteurs de mana presque stables. L'origine de l'incident
étant un abus sur leur utilisation.

Remarque : le mana est une source inépuisable d'énergie. En théorie, il
suffirait de brûler un artefact ou suffisamment de minerai pour initier le
processus de production d'énergie. Le réacteur pourrait ensuite fonctionner
indéfiniment. La raion pour laquelle il fallait constamment user du stock
venait des soucis de fuite de mana (avant la mise au point des piles de mana,
il n'existait aucun moyen réellement fiable pour contenir le mana, et lors de
la combustion, les champs magnétiques ne peuvent être utilisé efficacement).

Entre la mise au point des tout premiers réacteurs et les années noires, il
s'écoulera plusieurs siècles. L'évolution entre les premiers et derniers
réacteurs tiendra essentiellement dans :
* l'accroissement de leur puissance,
* la réduction des pertes (fuites) de mana,
* l'évolution des techniques de stockage du mana.

### Fin de la guerre

À l'origine du conflit se trouvent d'une part une situation de guerre froide
entre les plus grandes puissances, et d'autre part la mise au point d'un
réacteur de mana particulièrement puissant. Le dirigeant se trouvant derrière
la mise au point de ce réacteur développera un projet d'usage militaire des
performances de cette centrale, dans un projet "trop" secret aux yeux des
autres grandes puissances.

La guerre froide finira par exploser dans l'une des régions les plus riches et
développées du monde, celle qui sera le futur Désert de Cristal.

L'incident provoquera d'abord une hausse quasi instantanée de la température
de plusieurs centaines de milliers de degrés dans un rayon de quelques dizaines
de kilomètres. L'onde de choc balayera tout dans une rayon de près de mille
kilomètres. Le dérèglement de température se propagera ensuite dans les jours
qui suivent. Au fur et à mesure que ce dernier s'étalera, la hausse de
température sera de moins en moins significative. Il n'y a que la zone du
(futur) désert de Cristal qui sera réellement soumis à des témpératures
extrêmes (plusieurs dizaines de milliers de degré en son centre, quelques
centaines de degrés à ses limites.
La température suivra ensuite une baisse régulière (style 1/x) sur quelques
siècles jusqu'à un retour à la normal bien après la fin du 3e cycle.

Remarque l'incident aura pu causer un "trou" dans l'atmosphère, justifiant les
dérèglements météorologiques dans les régions avoisinantes.

La région qui deviendra le désert de cristal était celle où eu lieu le conflit,
et celle qui était la plus riche du monde, partagée entre les nations les plus
puissantes de l'époque.

L'explosion du réacteur de mana dispersa également sa matière première partout
dans l'atmosphère et se dispersera jusque dans l'organisme des êtres vivants,
leur conférant une maîtrise plus ou moins poussée de la magie.
Dans la zone du désert de cristal, toutefois, les doses de mana perçuent par
les organismes seront tellement élevées que certaines mutations physiques
pourront être observées. (C'est au cours des premiers siècles que les rumeurs
sur l'existence de guivres et autres créatures monstrueuses dans le désert de
Cristal).

À l'explosion du réacteur, la dispersion de mana sera perçue par Heimdall et
Fafnir. Le premier en avertira Bahamut, tandis que le second commencera à
porter son attention sur le monde de Gaïa.
