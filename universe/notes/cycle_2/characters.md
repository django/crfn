Personnages
===========

Les ancêtres de Lucian von Ruthven
----------------------------------

### Karl Tesla

Créateur du premier réacteur de mana "à peu près" stable, sa famille, ainsi que
celle de ses collaborateurs, étaient retenue en otage comme moyen de pression
pour les faire travailler sur un projet qu'il savait être à usage militaire, et
potentiellement dangereux pour l'humanité dans son ensemble.

Lorsque le conflit entre les propriétaires du réacteur de Karl et les autres
super-puissances commença à s'enliser, les supérieurs de Karl décidèrent d'user
de l'arme qu'ils avaient créé à partir du générateur de Karl, mais celle-ci
n'étant pas stable, l'explosion dévasta toute la région dans un rayon de 100km.
Karl se trouvant proche du centre de l'explosion, il n'eut même pas le temps de
souffrir...

### Annah Tesla (future von Ruthven)

Fille de Karl et Milena Tesla. Elle, sa mère, et les autres familles des
ingénieurs étaient détenues à une centaine de kilomètre du site où travaillait
Karl Tesla. Elles seront libérées au moment où l'explosion de la centrale aura
lieu par Dietrich von Ruthven, alors agent secret au service de l'une des
armées ennemies. Lui, les deux femmes et quelques autres survivants parvinrent
à s'abriter dans des souterrains du chaos qui déferlait sur la région.

### Dietrich von Ruthven

Originaire d'une ville située en plein dans une zone de conflit, Dietrich a
survécu en formant une bande de mercenaires avec une bande de rescapés.
Cependant, au fond de son cœur, il déteste ces conflits qui sont à l'origine
de la disparition de sa famille. Il cache son amertume derrière un humour
percutant, et laissera toujours une chance de survivre à ses adversaires en
leur offrant l'opportunité de rejoindre son groupe. (Il n'a d'ailleurs jamais
considéré les gens qu'ils affrontaient comme des ennemis, considérant qu'ils y
étaient forcés par les circonstances).

C'est lui qui va sauver Mira Tesla lors de l'explosion du réacteur de Karl
Tesla, et qui sera le futur époux de Mira. Ils fondèrent ensuite l'une des
rares colonies de survivant, dans les plaines du Nord, dont le climat s'était
sensiblement réchauffé.

### Victor von Ruthven

Fils de Dietrich et Mira von Rutvhen, Ce sera l'arrière grand père de Lucian
von Ruthven. Ce sera également le premier à rencontrer Freya, l'esprit (ou être
féé) des grands fleuves du Nord, qui sera la future épouse de Lucian von
Ruthven.

Victor est officiellement reconnu comme le premier dirigeant des villages
libres du Grand Nord, alors que certains historiens en donnent la paternité à
Dietrich von Ruthven. Ce qui est sûre, c'est l'importance de la rencontre de
Victor avec Freya dans le choix de l'endroit où sera créé le premier village de
la colonie.

### Freya

Freya est un esprit supérieur de la race des fée, rattachée aux fleuves et
cours d'eau du Nord du Westeard. Elle est considérée comme une nymphe, et le
peu de gens à l'avoir jamais croisé s'accordent pour dire qu'elle est d'une
beauté extrême.

Elle arrive à la fin du second cycle, envoyée avec ses sœurs dans le Nord de ce
qui sera plus tard le Westeärd pour tenter d'atténuer les conséquences de la
course au pouvoir des grandes puissances. C'est pour cette raison que Freya
aidera les von Ruthven à s'installer dans le Grand Nord.
