Exploration des confins de la galaxie
=====================================

Rencontre avec les dieux alors même que certains mortels ont presque autant de
pouvoir qu'eux.

Les portails spatio temporelles
-------------------------------

Longtemps envisagés par toute sorte de théories, l'existence de ces portails n'a
été observée pour la première fois qu'à partir du moment où les voyages en
dehors du système stellaire sont devenus accessibles. Ils résultent de la
déformation des 4 dimensions provoquées par l'attraction gravitationnelle d'un
trou noir lorsque celui-ci atteint une masse suffisante. Un portail ne peut
envoyer que vers un seul autre portail, et sans intervention extérieure, il
disparaît en quelques 10^-43 secondes et sa dimension est infinitésimale. Les
chercheurs réussirent quand même à envoyer des sondes reliés par intrication
quantique, de sorte qu'il soit toujours possible de communiquer avec et
contrôler à distance la sonde. Une progression sur plusieurs siècles mènera à
leur exploitation pour les déplacements quasi instantanés d'un système stellaire
à un autre. La totale maîtrise du phénomène permettra beaucoup plus tard la
génération de portail depuis un vaisseau pour atterrir à l'endroit voulu.