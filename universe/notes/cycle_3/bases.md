le mana devient source de la magie
==================================

Lucian von Ruthven
------------------

Lucian von Ruthven est détenteur d'un fragment de l'âme du mana, issu du noyau
du mana, sous la forme d'un pendentif qu'il porte autour du cou. Dans sa
jeunesse, il découvrira une réplique de moindre puissance du réacteur de mana,
ainsi que leur possesseurs. Lorsque Lucian s'approchera du noyau du réacteur,
son pendentif entrera en résonance avec le mana qu'il contient.

L'extrait d'âme du mana va alors fusionner dans son torse et le réacteur va
perdre sa stabilité, entraînant son explosion. Celle-ci va détruire la cité que
le réacteur alimentait en énergie ainsi que la région alentours, et Lucian sera
le seul survivant, sauvé de justesse par Heimdall.

Celui-ci, agent de Bahamut, courrait après les fragments de mana depuis de
nombreux siècles et c'est lorsque celui de Lucian entra en résonance avec le
réacteur que Heimdall le repéra.

Fafnir enverra à ce moment là ses premiers agents sur le monde de Gaïa. À ce
moment là, leur rôle est surtout de collecter des infos sur la présence de
fragments de Mana sur le monde de Gaïa. Parmis ces agents se trouvent notamment
Cernobog et Nidhogg. Nihdogg sera le plus puissant des Seigneurs du Chaos, mais
il se concentrera surtout sur la création d'une race possédant la puissance des
fiélons sans leur caractère en mêlant leur sang avec celui de mortels du monde
de Gaïa.

La fusion de l'âme du mana avec le corps de Lucian eut pour effet d'accroître
ses capacités et le rendre quasiment immortel: pratiquement l'égal des dieux.
Seul signe extérieur du changement: ses cheveux blancs.

Lorsque Heimdall sauve Lucian, il le prévient qu'il n'est pas le seul à
s'intéresser au monde de Gaïa. Il l'informe que les fragments du cœur du Mana
serait le seul moyen pour les dieux de retrouver leur pouvoir, et que Fafnir
rêve de mettre la main dessus. À la question de Lucian, Heimdall lui répond que
les autres dieux ont tiré un trait sur l'usage de la magie, estimant que d'une
part ils ont atteint un niveau où ils n'en ont plus besoin, et d'autre part que
l'univers peut poursuivre son développement sans leur intervention.
Si Lucian reçois les alertes de Heimdall, il reste sceptique sur la position de
Bahamut et des autres dieux. À ce moment là, il craint que le monde de Gaïa
n'attire des forces qui viendraient s'affronter pour le contrôle du pouvoir.
Dans un premier temps, il ne fait rien, écrasé par le désespoir de la perte de
ses amis, mais lorsqu'il reprendra le dessus, il commencera à chercher à
retrouver le dessus. Sa (re)rencontre avec Freya lui sera d'une grande aide, car
elle pourra l'aider de ses connaissances. Il élaborera ainsi "la" prophétie qui
permettra au monde de rester conscient d'une menace latente.

Lucian von Ruthven créé le premier royaume stable depuis les années noirs et
rencontre le père de Garah Pulgren. Il lui parle de la prophétie dont il est le
porteur, et ensemble, ils conviennent qu'un empire fort en Westeard serait un
bon rempart contre les hordes du Chaos. Il était convenu que le père de Garah
se charge de jeter les bases de cet empire, mais le chef de la bande dont il
était membre vit ce projet comme une tentative pour le supplanter et fit sont
possible pour éliminer le père de Garah. Ce dernier se chargera de fonder le
royaume qui fera office de socle au futur empire, comme le raconte la nouvelle
dédiée.

Les descendants de Garah finirent par oublier leurs liens avec Lucian, puis
finirent par convoiter son royaume qui les coupaient d'un accès important à la
grande mer intérieure. L'un d'entre eux instigue la révolte qui mettra fin au
royaume de Lucian. Dracul incarnera la haine de la famille à l'égard de
l'Angara. Fafnir en profitera pour tenter de s'emparer de son âme et provoquera
la tentative d'assassinat sur Freya. Falke incarnera le pardon à l'égard de
l'Angara, considérant que ce n'est la faute que d'un seul homme, tandis que
Elanor, qui fut l'épouse de Garah, alla corriger son descendant et prendre en
charge le fils de ce dernier.

Première grande guerre contre le Chaos
--------------------------------------

Les hordes de Fafnir tentent de s'emparer du Westeard.

L'Empire fondé quelques siècles plus tôt par Garah résistera, mais la victoire
elle-même est acquise grâce au sacrifice des nains, qui font s'effondrer leur
plus important royaume souterrain sur l'une des armées du Chaos, l'amputant de
pratiquement un tiers de sa puissance. Après cela, les nains vinrent en soutien
des fils de Khan et leurs forces conjuguées leurs permit d'anéantir un second
tiers des forces du Chaos.

L'héritier de Falke vainquit l'héritier de son jumeau (Dracul). Ce sont les
deux fils de Lucian. L'héritier de Dracul étant l'un des généraux du seigneur
du Chaos qui devait remporter la cité Delta par l'ouest, sa défaite précipita
celle des hordes du Chaos.

Suite à cet échec, les Seigneurs du Chaos prennent le temps de reformer leurs
armées. Arrivée de Xenuvia dans leur rang, qui doit faire ses preuves. Elle
provoque une révolution au Sora et s'infiltre progressivement dans le pouvoir
de l'empire et fini par se substituer à Gaia elle-même comme déesse des junins.

Une fois installée, Xenuvia fera échouer sa révolution, mais sera déjà placée
au sommet du Sora, qu'elle offrira sur un plateau à Fafnir. Par la suite, elle
lancera sont empire à la conquête de tout le Tochitoyo, puis commencera à
former l'armée d'invasion de l'Est qui déferlera sur le Westeard un bon siècle
plus tard.

Dans l'espoir de la contrer, Den se mêlera à la résistance formée par Xenuvia.
Quand cette résistance échouera, Den se servira des guildes de voleurs pour
former un contre pouvoir discret. C'est la raison de ses liens avec le chef de
Jotaro.

Seconde grande guerre contre le Chaos
-------------------------------------

Le rôle de Jotaro dans la seconde grande guerre sera d'anéantir tout espoir de
repli des forces de Xenuvia sur le Tochitoyo, et de libérer le Sora des fiélons
qui profitaient de l'absence de Xenuvia pour imposer leurs lois sur le peuple.

Principaux empires
------------------

### Angara

Ancien royaume d'Enraska. Garah avait installé son château à proximité de la
cité indépendante de Delta, et plusieurs fermes état avaient accepté d'être
vassale du jeune roi, socle du futur empire.