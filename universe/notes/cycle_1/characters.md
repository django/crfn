Les dieux
=========

Les dieux n'ont pas d'apparence prédéfinie, mais une apparence qu'ils se
choisissent et qui reflète leur personnalité ou l'image qu'ils veulent donner
d'eux. Ainsi, ils changent très rarement d'apparence, et s'ils le font ils
prennent le temps et le font de façon très réfléchie.

Les dieux sont relativement peu nombreux et répartis dans tout l'univers. Leur
faculté à communiquer par la pensée et leur conscience collective fait qu'ils
peuvent garder contact quelque soit la distance.

Cette relation entre les dieux ne sera ébranlée que par la fracture de l'univers
qui a abouti à la cosmologie actuelle: la conséquence est que le lien entre
Gaïa, qui s'est retrouvée isolée, et ses frères et sœurs, sera fortement
atténué, mais juste suffisant pour lui permettre de communiquer de vagues
sentiments.

Parmi les dieux, quatre se distinguent: les premiers nés. Il s'agit de Bahamut,
roi des dieux, Fafnir l'impétueux, Gaïa la curieuse et Danah la changeante.
Selon la légende, ce seraient les quatre premiers dieux à être arrivé dans
l'univers et à avoir commencé à le remplir.

Ce sont également les quatre seuls à déroger à au moins une des règles régissant
les dieux: Bahamut est capable de créer à partir du néant et de détruire,
Fafnir est plus puissant, Danah ne s'impose pas d'apparence, et Gaïa à plus de
liberté dans ses créations, qui sont les plus riches de l'univers.

Les dieux possèdent la faculté de former des êtres vivants, mais ceux-ci seront
limités dans leurs possibilités et leurs capacités. Mais la principale
contrainte est qu'ils ne peuvent former d'êtres dotés d'un libre arbitre, 
c'est-à-dire qu'ils ne seront pas capable de prendre d'initiative par eux-mêmes
et resterons assujettis aux caractéristiques qui leur ont été affectées à leur
création. Les dieux perçoivent également les pensées des êtres qu'ils ont créé
et qui sont tournées vers eux. 
En général, ce sont des communications simples, étant donné les limites de leurs
créations, mais pour Gaïa, elle recevra des prières bien plus élaborées.
(cf. cycle 3)

Bahamut
-------

Roi des dieux, son apparence reflète son immense sagesse et laisse transparaître
une très grande force. Ses traits rappellent ceux d'un lion au regard pénétrant
et paisible.

Très réfléchit, ses décisions peuvent être prises après des années et des années
de maturation. Il est cependant capable de faire ses choix en quelques instants
lorsque la situation l'exige.

Fafnir
------

Violent et impulsif, il a pris l'apparence d'un dragon, et des flammes semblent
danser dans son regard. Guerrier né, il est le plus fort des dieux et les mondes
qu'il a créé ne son que feux et flammes.

C'est également lui qui est à l'origine des fiélons, qu'il a initialement créé
pour les plonger dans d'éternelles luttes de pouvoir.

Gaïa
----

Si on la rencontre sans savoir qui elle est, elle donnera l'impression
d'une jeune fille ingénue et superficielle. Pourtant, en la connaissant mieux,
on réalise l'étendue de sa sagesse, et surtout son grand savoir, hérité de tout
ce qu'elle a fait par curiosité.

Sa grande beauté réside plus dans la douceur de son regard que dans son
apparence générale.

Contrairement aux autres dieux, elle a créé peut de monde, et s'est consacrée à
sa principale œuvre, qui est sans conteste le monde le plus aboutit de l'univers.

Danah: surnommée la changeante, c'est la seule déesse à changer régulièrement
d'apparence, voir même à être restée à l'état brut d'âme. Autrement, elle
revient généralement à l'apparence d'une jeune femme brune aux yeux verts plein
de malice.

Constamment en déplacement, elle a créé nombre de monde, mais n'en a pas achevé
un seul.

Lors du conflit entre les dieux, les révélations de Fafnir vont grandement la
perturber. Elle rejoindra le camp de Fafnir et se perdra dans l'univers à sa
défaite. Son amitié pour Gaïa la poussera à rendre visite à son monde, beaucoup
plus tard, ce qui en fera l'un des artisans de la paix entre les dieux et les
mortels lors du conflit final (cf. dernier cycle).
