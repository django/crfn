Évolution progressive des connaissances:
========================================

Avant la première grande guerre: pur médiéval fantastique. Entre les deux
guerres: évolution progressive vers du steampunk avec d'abord la découverte de
la poudre, puis la maîtrise de la vapeur peut de temps après la seconde guerre
(ne pas oublier qu'il y a un gel des progrès tout de suite après la guerre à
cause de la famine, des révolutions, etc. Pendant quelques dizaines d'années).
Période pure steampunk pendant quelques siècles et début de
l'industrialisation, puis découverte et maîtrise de l'électricité, et tout ce
qui s'en suit sur les siècles qui suivent.

Il n'y aura que la notion d'informatique qui apparaitra sur du steampunk tardif
avec la mise au point de la mécanique basée sur la vapeur (les premiers ordis
avec écran devront sans doute attendre l'électricité pour apparaître).
