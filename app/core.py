# -*- coding: utf-8 -*-

import inspect

from flask import Flask

from utils import isview, build_routes

class App(Flask):
    def add_routes(self, routes):
        for route in routes:
            self.add_url_rule(
                route['rule'],
                endpoint=route['ep'],
                view_func=route['func'],
                methods=[route.get('meth', 'GET')]
            )

class View(object):
    """Base class for all views"""
    def routes(self):
        meths = inspect.getmembers(self, predicate=inspect.ismethod)
        routes = []
        prefix = self.__class__.__name__.replace('View', '').lower()
        
        if callable(self):
            routes.append({
                'rule': '/{}'.format(prefix),
                'ep': prefix,
                'func': self,
                'meth': ['GET']  # By default only 'GET' is enabled
            })

        for meth in meths:
            for name, func in meth:
                if isview(name):
                    routes.append(build_routes(func, rule_prefix=prefix))
        return routes
